# Retro A500 style computer


## Description
These are the files to build a retro Amiga 500 style PicoMite VGA computer.

## Authors and acknowledgment
Thanks to Geoff Graham, Peter Mather and Mick Ames for creating the great PicoMite VGA firmware.

## License
Creative Commons Allocation - No Commercial Use CC BY-NC
